Context Inject
------------

This module provides a context reactions to inject HTML snippets and
libraries to page.

 * For a full description of the module, visit the [project page](https://www.drupal.org/project/context_inject).
 * To submit bug reports and feature suggestions, or to track changes, use the [issue queue](https://www.drupal.org/project/issues/context_inject).

TABLE OF CONTENTS
---------------------

* Requirements
* Installation
* Configuration
* Maintainers


REQUIREMENTS
------------
This module requires the following modules:

- [Context](https://www.drupal.org/project/context)


INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


CONFIGURATION
-------------

## Inject snippet
* Create or edit any context
* Add "Attach snippet" reaction
* Put full HTML code inside textarea and choose placement position (page Top or Bottom)

## Inject library
* Create or edit any context
* Add "Attach library" reaction
* Put comma separated list of full library names prefixed with theme or module

MAINTAINERS
-----------

Current maintainers:
 * Sergei Sergin (kruhak) - https://www.drupal.org/u/kruhak

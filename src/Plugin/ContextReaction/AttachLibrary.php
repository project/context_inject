<?php

namespace Drupal\context_inject\Plugin\ContextReaction;

use Drupal\Component\Render\MarkupInterface;
use Drupal\context\ContextReactionPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a reaction to attach theme or module library to page.
 *
 * @ContextReaction(
 *   id = "context_inject_library",
 *   label = @Translation("Inject library")
 * )
 */
class AttachLibrary extends ContextReactionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function summary(): MarkupInterface {
    return $this->t('Gives you ability to attach library from theme or module to page');
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {}

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $defaults = parent::defaultConfiguration();
    $defaults['library'] = '';
    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $form['library'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Library name'),
      '#description' => $this->t('Enter the full library name with module or theme prefix. Separate multiple libraries by comma.'),
      '#attributes' => [
        'placeholder' => 'theme_name/library_name',
      ],
      '#default_value' => $this->configuration['library'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    $this->configuration['library'] = $form_state->getValue('library');
  }

}

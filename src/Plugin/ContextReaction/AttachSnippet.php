<?php

namespace Drupal\context_inject\Plugin\ContextReaction;

use Drupal\Component\Render\MarkupInterface;
use Drupal\context\ContextReactionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

/**
 * Provides a reaction to attach CSS/JS snippet to page.
 *
 * @ContextReaction(
 *   id = "context_inject_snippet",
 *   label = @Translation("Inject HTML snippet")
 * )
 */
class AttachSnippet extends ContextReactionPluginBase {

  const POSITION_TOP = 'top';

  const POSITION_BOTTOM = 'bottom';

  /**
   * {@inheritdoc}
   */
  public function summary(): MarkupInterface {
    return $this->t('Gives you ability to attach Javascript to page');
  }

  /**
   * {@inheritdoc}
   */
  public function execute(string $position = 'bottom'): array {
    return [
      '#markup' => Markup::create($this->configuration['snippet']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $defaults = parent::defaultConfiguration();
    $defaults['snippet'] = '';
    $defaults['attach_position'] = static::POSITION_BOTTOM;
    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $form['snippet'] = [
      '#type' => 'textarea',
      '#title' => $this->t('HTML Snippet'),
      '#rows' => 10,
      '#default_value' => $this->configuration['snippet'],
    ];

    $form['attach_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Position'),
      '#options' => [
        static::POSITION_BOTTOM => $this->t('Bottom'),
        static::POSITION_TOP => $this->t('Top'),
      ],
      '#default_value' => $this->configuration['attach_position'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    $this->configuration['snippet'] = $form_state->getValue('snippet');
    $this->configuration['attach_position'] = $form_state->getValue('attach_position');
  }

}
